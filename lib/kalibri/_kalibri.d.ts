import { IAfterInterceptor, IBeforeInterceptor, IDeleteRequestConfig, IGetRequestConfig, IPostRequestConfig, IRequestConfig, ISuccessResponse } from './_types';
export interface IKalibriConfig {
    baseUrl: string;
    timeout: number;
    headers: {
        [n: string]: string;
    };
    ignoredHeaders: {
        key: string;
        times: number;
    }[];
}
export declare const initConfig: IKalibriConfig;
export declare class Kalibri {
    private config;
    protected beforeRequestInterceptor: IBeforeInterceptor;
    protected afterRequestInterceptor: IAfterInterceptor;
    constructor({ baseUrl, timeout, headers, }?: Partial<Omit<IKalibriConfig, 'ignoredHeaders'>>);
    private get headers();
    createInstance(options?: Partial<Omit<IKalibriConfig, 'ignoredHeaders'>>): Kalibri;
    getConfig(): IKalibriConfig;
    setBaseUrl(url: string): void;
    addHeaders(headers: {
        [n: string]: string;
    }): void;
    removeHeaders(headers: string[]): {
        [K in typeof headers[number]]: string;
    };
    removeHeaders(headers: string): string;
    setAfterRequestInterceptor(afterRequestInterceptor: IAfterInterceptor): void;
    setBeforeRequestInterceptor(beforeRequestInterceptor: IBeforeInterceptor): void;
    ignoreHeaders(ignoredHeaders: {
        key: string;
        times: number;
    }[]): void;
    stopIgonringHeaders(keys: string | string[]): void;
    private removeIgnoredHeader;
    request<T>(requestConfig: Partial<IRequestConfig>): Promise<ISuccessResponse<T>>;
    get<T>(url: string, requestConfig?: IGetRequestConfig): Promise<ISuccessResponse<T>>;
    post<T>(url: string, requestConfig?: IPostRequestConfig): Promise<ISuccessResponse<T>>;
    patch<T>(url: string, requestConfig?: IPostRequestConfig): Promise<ISuccessResponse<T>>;
    put<T>(url: string, requestConfig?: IPostRequestConfig): Promise<ISuccessResponse<T>>;
    delete<T>(url: string, requestConfig?: IDeleteRequestConfig): Promise<ISuccessResponse<T>>;
}
