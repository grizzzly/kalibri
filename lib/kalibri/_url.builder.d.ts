export declare class URLBuilder {
    private finalUrl;
    constructor(baseUrl: string);
    path(path: string): this;
    query(params: {}): this;
    build(): string;
}
