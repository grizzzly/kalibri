"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Kalibri = exports.initConfig = void 0;
const _types_1 = require("./_types");
const _helpers_1 = require("./_helpers");
const _url_builder_1 = require("./_url.builder");
const _client_adapter_1 = require("./adapters/_client.adapter");
const _server_adapter_1 = require("./adapters/_server.adapter");
exports.initConfig = {
    baseUrl: '/',
    timeout: 10000,
    headers: {},
    ignoredHeaders: [],
};
class Kalibri {
    constructor({ baseUrl = '/', timeout = 10000, headers = {}, } = exports.initConfig) {
        this.config = exports.initConfig;
        this.beforeRequestInterceptor = (config) => { };
        this.afterRequestInterceptor = (requestResult) => { };
        if (typeof baseUrl !== 'string') {
            throw new Error(`Expected baseUrl as string but got ${typeof baseUrl}`);
        }
        if (headers === null ||
            typeof headers !== 'object' ||
            Array.isArray(headers)) {
            throw new Error(`Expected headers as array but got ${typeof headers}`);
        }
        if (typeof timeout !== 'number') {
            throw new Error(`Expected timeout as number but got ${typeof timeout}`);
        }
        this.config = {
            baseUrl: baseUrl || exports.initConfig.baseUrl,
            timeout: timeout || exports.initConfig.timeout,
            headers: headers || exports.initConfig.headers,
            ignoredHeaders: [],
        };
    }
    get headers() {
        const $headers = Object.assign({}, this.config.headers);
        const $newIgnoredHeaders = [];
        for (const ignoredHeader of this.config.ignoredHeaders) {
            const { key, times } = ignoredHeader;
            if (key in $headers) {
                delete $headers[key];
            }
            ignoredHeader.times = times - 1;
            if (ignoredHeader.times > 0) {
                $newIgnoredHeaders.push(ignoredHeader);
            }
        }
        this.config.ignoredHeaders = $newIgnoredHeaders;
        return $headers;
    }
    createInstance(options) {
        return new Kalibri(options);
    }
    getConfig() {
        return JSON.parse(JSON.stringify(this.config));
    }
    setBaseUrl(url) {
        if (url[url.length - 1] !== '/') {
            url += '/';
        }
        this.config.baseUrl = url;
    }
    addHeaders(headers) {
        if (headers === null || typeof headers !== 'object') {
            throw new Error(`Exprcted headers as object, but got: ${headers}`);
        }
        this.config.headers = Object.assign(Object.assign({}, this.config.headers), headers);
    }
    removeHeaders(headers) {
        const removeHeader = (key) => {
            const headerText = this.config.headers[key];
            delete this.config.headers[key];
            return headerText;
        };
        if (typeof headers === 'string') {
            return removeHeader(headers);
        }
        if (Array.isArray(headers)) {
            const res = {};
            headers.forEach((key) => {
                const headerText = removeHeader(key);
                res[key] = headerText;
            });
            return res;
        }
        throw new Error('');
    }
    setAfterRequestInterceptor(afterRequestInterceptor) {
        this.afterRequestInterceptor = afterRequestInterceptor;
    }
    setBeforeRequestInterceptor(beforeRequestInterceptor) {
        this.beforeRequestInterceptor = beforeRequestInterceptor;
    }
    ignoreHeaders(ignoredHeaders) {
        this.config.ignoredHeaders = [
            ...this.config.ignoredHeaders,
            ...ignoredHeaders,
        ];
    }
    stopIgonringHeaders(keys) {
        if (typeof keys === 'string') {
            this.removeIgnoredHeader(keys);
        }
        if (Array.isArray(keys)) {
            keys.forEach((key) => {
                this.removeIgnoredHeader(key);
            });
        }
    }
    removeIgnoredHeader(key) {
        this.config.ignoredHeaders = this.config.ignoredHeaders.filter((ignoredHeader) => ignoredHeader.key !== key);
    }
    request(requestConfig) {
        const $requestConfig = {
            method: requestConfig.method || _types_1.REQUEST_METHODS.GET,
            url: new _url_builder_1.URLBuilder(this.config.baseUrl)
                .path(requestConfig.url || '')
                .query(requestConfig.query || {})
                .build(),
            headers: Object.assign(Object.assign({}, this.headers), requestConfig.headers),
            query: requestConfig.query || {},
            timeout: requestConfig.timeout || this.config.timeout,
            body: requestConfig.body,
            responseType: requestConfig.responseType || '',
        };
        const adapter = _helpers_1.isServer() ? new _server_adapter_1.default() : new _client_adapter_1.default();
        return adapter.request({
            requestConfig: $requestConfig,
            interceptors: {
                beforeRequestInterceptor: this.beforeRequestInterceptor,
                afterRequestInterceptor: this.afterRequestInterceptor,
            },
        });
    }
    get(url, requestConfig = {}) {
        return this.request(Object.assign(Object.assign({}, requestConfig), { url, method: _types_1.REQUEST_METHODS.GET }));
    }
    post(url, requestConfig = {}) {
        return this.request(Object.assign(Object.assign({}, requestConfig), { url, method: _types_1.REQUEST_METHODS.POST }));
    }
    patch(url, requestConfig = {}) {
        return this.request(Object.assign(Object.assign({}, requestConfig), { url, method: _types_1.REQUEST_METHODS.PATCH }));
    }
    put(url, requestConfig = {}) {
        return this.request(Object.assign(Object.assign({}, requestConfig), { url, method: _types_1.REQUEST_METHODS.PUT }));
    }
    delete(url, requestConfig = {}) {
        return this.request(Object.assign(Object.assign({}, requestConfig), { url, method: _types_1.REQUEST_METHODS.DELETE }));
    }
}
exports.Kalibri = Kalibri;
