import { IFinalServerConfig, ISuccessResponse } from '..';
export declare class ServerService {
    private parseResponseText;
    private createSuccessResponse;
    private createErrorResponse;
    request<T>(options: IFinalServerConfig): Promise<ISuccessResponse<T>>;
}
