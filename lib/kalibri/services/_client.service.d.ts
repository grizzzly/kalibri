import { IFinalClientConfig, ISuccessResponse, IBeforeInterceptor, IAfterInterceptor, IErrorResponse } from '..';
export declare class ClientService {
    private beforeRequestInterceptor;
    private afterRequestInterceptor;
    setAfterRequestInterceptor(afterRequestInterceptor: IAfterInterceptor): void;
    setBeforeRequestInterceptor(beforeRequestInterceptor: IBeforeInterceptor): void;
    parseResponseText(xhr: XMLHttpRequest): any;
    successResponse<T>(xhr: XMLHttpRequest): ISuccessResponse<T>;
    errorResponse(xhr: XMLHttpRequest, message?: string): IErrorResponse;
    request<T>(requestConfig: IFinalClientConfig): Promise<ISuccessResponse<T>>;
}
