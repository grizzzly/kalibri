"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerService = void 0;
var http = require("http");
var ServerService = (function () {
    function ServerService() {
    }
    ServerService.prototype.parseResponseText = function (data) {
        return data[0] === '{' ? JSON.parse(data) : data;
    };
    ServerService.prototype.createSuccessResponse = function (data, res) {
        var _a = res.statusCode, statusCode = _a === void 0 ? 0 : _a, headers = res.headers, _b = res.statusMessage, statusText = _b === void 0 ? '' : _b;
        return {
            headers: headers,
            statusText: statusText,
            ok: true,
            status: statusCode,
            data: this.parseResponseText(data.toString()),
        };
    };
    ServerService.prototype.createErrorResponse = function (res, data, message) {
        if (message === void 0) { message = ''; }
        var _a = res.statusCode, status = _a === void 0 ? 0 : _a, _b = res.statusMessage, statusText = _b === void 0 ? '' : _b, headers = res.headers;
        return {
            ok: false,
            status: status,
            statusText: statusText,
            headers: headers,
            message: message,
            data: this.parseResponseText(data.toString()),
        };
    };
    ServerService.prototype.request = function (options) {
        var _this = this;
        if (options.data !== null) {
            Object.assign(options, {
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': options.data.length,
                },
            });
        }
        return new Promise(function (resolve, reject) {
            var req = http.request(options, function (res) {
                res.on('data', function (data) {
                    var _a = res.statusCode, statusCode = _a === void 0 ? 0 : _a;
                    if (statusCode >= 200 && statusCode < 400) {
                        return resolve(_this.createSuccessResponse(data, res));
                    }
                    reject(_this.createErrorResponse(res, data));
                });
            });
            req.on('error', function (error) {
                console.log(error);
                reject(error);
            });
            if (options.data) {
                req.write(options.data);
            }
            req.end();
        });
    };
    return ServerService;
}());
exports.ServerService = ServerService;
