"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.REQUEST_METHODS = void 0;
var REQUEST_METHODS;
(function (REQUEST_METHODS) {
    REQUEST_METHODS["GET"] = "GET";
    REQUEST_METHODS["POST"] = "POST";
    REQUEST_METHODS["PATCH"] = "PATCH";
    REQUEST_METHODS["DELETE"] = "DELETE";
    REQUEST_METHODS["PUT"] = "PUT";
})(REQUEST_METHODS = exports.REQUEST_METHODS || (exports.REQUEST_METHODS = {}));
