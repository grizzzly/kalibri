"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http = require("http");
const https = require("https");
const _base_adapter_1 = require("./_base.adapter");
const getHttp = (url) => {
    return url.includes('https://') ? https : http;
};
class ServerAdapter extends _base_adapter_1.AAdapter {
    parseResponseText(data) {
        return data[0] === '{' ? JSON.parse(data) : data;
    }
    createSuccessResponse(data, res) {
        const { statusCode = 0, headers, statusMessage: statusText = '' } = res;
        return {
            headers,
            statusText,
            ok: true,
            status: statusCode,
            data: this.parseResponseText(data.toString()),
        };
    }
    createErrorResponse(res, data, message = '') {
        const { statusCode: status = 0, statusMessage: statusText = '', headers, } = res;
        return {
            ok: false,
            status,
            statusText,
            headers,
            message,
            data: this.parseResponseText(data.toString()),
        };
    }
    createFinalConfig(requestConfig) {
        const { url, method, headers, timeout, body = {}, responseType, } = requestConfig;
        const finalUrl = new URL(url);
        const data = typeof body === 'object' && Object.entries(body).length
            ? new TextEncoder().encode(JSON.stringify(Object.assign({}, body)))
            : null;
        return {
            data,
            method,
            timeout,
            headers,
            responseType,
            origin: finalUrl.origin,
            hostname: finalUrl.hostname,
            path: finalUrl.pathname + finalUrl.search,
            port: finalUrl.port ? parseInt(finalUrl.port, 10) : undefined,
        };
    }
    request({ requestConfig, interceptors: { beforeRequestInterceptor, afterRequestInterceptor }, }) {
        beforeRequestInterceptor(requestConfig);
        const options = this.createFinalConfig(requestConfig);
        if (options.data !== null) {
            Object.assign(options, {
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': options.data.length,
                },
            });
        }
        return new Promise((resolve, reject) => {
            const req = getHttp(options.origin).request(options, (res) => {
                res.on('data', (data) => {
                    const { statusCode = 0 } = res;
                    if (statusCode >= 200 && statusCode < 400) {
                        return resolve(this.createSuccessResponse(data, res));
                    }
                    reject(this.createErrorResponse(res, data));
                });
            });
            req.on('error', (error) => {
                reject(error);
            });
            if (options.data) {
                req.write(options.data);
            }
            req.end();
        });
    }
}
exports.default = ServerAdapter;
