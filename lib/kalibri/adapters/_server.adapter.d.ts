import { ISuccessResponse } from '../_types';
import { AAdapter, IAdapterRequestOptions } from './_base.adapter';
export default class ServerAdapter extends AAdapter {
    private parseResponseText;
    private createSuccessResponse;
    private createErrorResponse;
    private createFinalConfig;
    request<T>({ requestConfig, interceptors: { beforeRequestInterceptor, afterRequestInterceptor }, }: IAdapterRequestOptions): Promise<ISuccessResponse<T>>;
}
