import { ISuccessResponse, IErrorResponse } from '../_types';
import { AAdapter, IAdapterRequestOptions } from './_base.adapter';
export default class ClientAdapter extends AAdapter {
    parseResponseText(xhr: XMLHttpRequest): any;
    successResponse<T>(xhr: XMLHttpRequest): ISuccessResponse<T>;
    errorResponse(xhr: XMLHttpRequest, message?: string): IErrorResponse;
    request<T>({ requestConfig, interceptors: { beforeRequestInterceptor, afterRequestInterceptor }, }: IAdapterRequestOptions): Promise<ISuccessResponse<T>>;
}
