import { IAdapterRequestConfig } from '..';
import { IAfterInterceptor, IBeforeInterceptor, ISuccessResponse } from '../_types';
export interface IAdapterRequestOptions {
    requestConfig: IAdapterRequestConfig;
    interceptors: {
        beforeRequestInterceptor: IBeforeInterceptor;
        afterRequestInterceptor: IAfterInterceptor;
    };
}
export declare abstract class AAdapter {
    abstract request<T>(options: IAdapterRequestOptions): Promise<ISuccessResponse<T>>;
}
