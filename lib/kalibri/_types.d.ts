export declare enum REQUEST_METHODS {
    GET = "GET",
    POST = "POST",
    PATCH = "PATCH",
    DELETE = "DELETE",
    PUT = "PUT"
}
export interface AnyObject {
    [name: string]: any;
}
export interface InitialConfig {
    baseUrl: string;
    timeout: number;
    lsHeadersKeys: string[];
}
export interface IRequestConfig {
    method: REQUEST_METHODS;
    url: string;
    headers: AnyObject;
    query: AnyObject;
    timeout: number;
    body: any;
    responseType: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | '';
}
export declare type IGetRequestConfig = Partial<Pick<IRequestConfig, 'headers' | 'query' | 'timeout' | 'responseType'>>;
export declare type IPostRequestConfig = Partial<Pick<IRequestConfig, 'headers' | 'query' | 'timeout' | 'body' | 'responseType' | 'responseType'>>;
export declare type IPatchRequestConfig = Partial<Pick<IRequestConfig, 'headers' | 'query' | 'timeout' | 'body' | 'responseType'>>;
export declare type IDeleteRequestConfig = Partial<Pick<IRequestConfig, 'headers' | 'query' | 'timeout' | 'responseType'>>;
export declare type IFinalClientConfig = Omit<IRequestConfig, 'query'>;
export declare type IAdapterRequestConfig = Omit<IRequestConfig, 'query'>;
export interface IFinalServerConfig {
    origin: string;
    hostname: string;
    port?: number;
    path?: string;
    headers?: AnyObject;
    timeout: number;
    method: REQUEST_METHODS;
    data: Uint8Array | null;
    responseType: IRequestConfig['responseType'];
}
export declare type GetRequest = (url: string, config?: IGetRequestConfig) => void;
export declare type PostRequest = (url: string, config?: IPostRequestConfig) => void;
export declare type PatchRequest = (url: string, config?: IPatchRequestConfig) => void;
export declare type DeleteRequest = (url: string, config?: IDeleteRequestConfig) => void;
export interface ISuccessResponse<T> {
    ok: boolean;
    status: number;
    statusText: string;
    headers: object | string;
    data: T;
}
export interface IErrorResponse {
    ok: boolean;
    status: number;
    statusText: string;
    headers: object | string;
    message: string;
    data: string;
}
export declare type IBeforeInterceptor = (config: IFinalClientConfig) => void;
export declare type IAfterInterceptor = (requestResult: ISuccessResponse<any>) => void;
export interface IKalibri {
    setBeforeRequestInterceptor: (interceptor: IBeforeInterceptor) => void;
    setAfterRequestInterceptor: (interceptor: IAfterInterceptor) => void;
    get: GetRequest;
    post: PostRequest;
    patch: PatchRequest;
    delete: DeleteRequest;
}
export interface ILsHeader {
    [title: string]: string;
}
export interface ILsKalibri {
    constantHeaders: ILsHeader;
    sessionHeaders: ILsHeader;
}
