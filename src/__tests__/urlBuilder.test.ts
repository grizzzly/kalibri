import { URLBuilder } from '../kalibri/_url.builder';

const BASE_URL = 'http://localhost:3000';

describe('Test url builder', () => {
  it('Should build url', () => {
    const url = new URLBuilder(BASE_URL)
      .path('bets')
      .query({ skill: 123 })
      .build();
    expect(url).toBe(`${BASE_URL}/bets?skill=123`);
  });
  it('Should build url', () => {
    const url = new URLBuilder(`${BASE_URL}/`)
      .path('/bets/')
      .query({ skill: 123 })
      .build();
    expect(url).toBe(`${BASE_URL}/bets/?skill=123`);
  });
  it('Should build url', () => {
    const url = new URLBuilder(BASE_URL).path('bets').build();
    expect(url).toBe(`${BASE_URL}/bets`);
  });
  it('Should build url', () => {
    const url = new URLBuilder(BASE_URL + '/').build();
    expect(url).toBe(`${BASE_URL}`);
  });
  it('Should build url', () => {
    const url = new URLBuilder(BASE_URL + '/').query({ bets: '123' }).build();
    expect(url).toBe(`${BASE_URL}?bets=123`);
  });
  it('Should build url', () => {
    const url = new URLBuilder(BASE_URL).path('http://google.com').build();
    expect(url).toBe(`http://google.com`);
  });
});
