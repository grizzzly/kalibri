import kalibri, { IKalibriConfig } from '../index';
import { initConfig } from '../kalibri';

import { createServer } from '../server/server';

const PORT = 3001;
const server = createServer(PORT);

interface ServerResponseData {
  status: string;
  headers: {};
}

beforeAll(async () => {
  await server.start();
});

afterAll(async () => {
  await server.close();
});

const serverUrl = `http://localhost:${PORT}/`;
kalibri.setBaseUrl(serverUrl);

describe('Test kalibri config', () => {
  const initialConfig = {
    ...initConfig,
    baseUrl: serverUrl,
  };
  it('Should initialize with config', () => {
    expect(kalibri.getConfig()).toMatchObject(initialConfig);
  });

  it('Should create new instance with new config', () => {
    const newConfig: IKalibriConfig = {
      ...initConfig,
      baseUrl: 'http://someurl.ru',
    };

    expect(kalibri.createInstance(newConfig).getConfig()).toMatchObject(
      newConfig,
    );
  });
});

describe('Testing headers', () => {
  it('shoud add, remove and ignore headers to config', () => {
    const headers = { Foo: 'bar', Bar: 'foo' };
    const ignoredHeaders = [
      { key: 'Foo', times: 1 },
      { key: 'Bar', times: 1 },
    ];

    kalibri.addHeaders(headers);
    expect(kalibri.getConfig().headers).toMatchObject(headers);

    kalibri.ignoreHeaders(ignoredHeaders);
    expect(kalibri.getConfig().ignoredHeaders).toEqual(ignoredHeaders);

    kalibri.removeHeaders(Object.keys(headers));
    expect(kalibri.getConfig().headers).not.toMatchObject(headers);
  });

  it('should send headers', async () => {
    const headers = { foo: 'bar' };
    kalibri.addHeaders(headers);

    const { data } = await kalibri.get<ServerResponseData>('/test');

    expect(data.headers).toEqual(expect.objectContaining(headers));

    kalibri.removeHeaders('foo');

    const { data: data1 } = await kalibri.get<ServerResponseData>('/test', {
      headers,
    });

    expect(data1.headers).toEqual(expect.objectContaining(headers));
  });

  it('should ignore headers', async () => {
    const headers = { foo: 'bar' };
    kalibri.addHeaders(headers);

    kalibri.ignoreHeaders([{ key: 'foo', times: 2 }]);

    const { data: data1 } = await kalibri.get<ServerResponseData>('/test');
    expect(data1.headers).not.toEqual(expect.objectContaining(headers));

    const { data: data2 } = await kalibri.get<ServerResponseData>('/test');
    expect(data2.headers).not.toEqual(expect.objectContaining(headers));

    const { data: data3 } = await kalibri.get<ServerResponseData>('/test');
    expect(data3.headers).toEqual(expect.objectContaining(headers));

    kalibri.removeHeaders('foo');
  });
});

describe('Testing http methods', () => {
  it('should return 200 on get request', async () => {
    const { status, data } = await kalibri.get<ServerResponseData>('/test');

    expect(status).toEqual(200);
  });

  it('should return sended data on post request', async () => {
    const { status, data } = await kalibri.post<ServerResponseData>('/test', {
      body: { status: 'test' },
    });

    expect(status).toEqual(200);
    expect(data).toHaveProperty('status', 'test');
  });

  it('Should return sended data on patch request', async () => {
    const { status, data } = await kalibri.patch<ServerResponseData>('/test', {
      body: { status: 'test' },
    });

    expect(status).toEqual(200);
    expect(data).toHaveProperty('status', 'test');
  });

  it('Should return ok on delete method', async () => {
    const { status, data } = await kalibri.delete<ServerResponseData>('/test');

    expect(status).toEqual(200);
  });
});

describe('Testing request config', () => {
  const formData = new FormData();
  formData.append('file', 'file data');

  it('should return form data', async () => {
    const { status, data } = await kalibri.post<ServerResponseData>('/test', {
      body: formData,
    });

    expect(status).toEqual(200);
    expect(data).toHaveProperty('file', 'file data');
  });

  it('should return headers', async () => {
    kalibri.addHeaders({
      abs: 'abs',
    });

    const { status, data } = await kalibri.post<ServerResponseData>('/test');

    expect(status).toEqual(200);
    expect(data.headers).toHaveProperty('abs', 'abs');
  });
});
