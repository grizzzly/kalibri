import { Kalibri } from './_kalibri';
export * from './_kalibri';
export * from './_types';

const kalibri = new Kalibri();
export default kalibri;
