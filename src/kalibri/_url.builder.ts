export class URLBuilder {
  private finalUrl = '';

  constructor(baseUrl: string) {
    // if last symbol equals to "/" then delete it
    const length = baseUrl.length - 1;
    this.finalUrl =
      baseUrl[length] === '/' ? baseUrl.substr(0, length) : baseUrl;
  }

  path(path: string) {
    if (path[0] === '/') {
      this.finalUrl += path;
    } else {
      this.finalUrl += '/' + path;
    }

    if (/(http(s?)):\/\//i.test(path)) {
      this.finalUrl = path;
    }

    return this;
  }

  query(params: {}) {
    if (Object.keys(params).length === 0) {
      return this;
    }

    if (this.finalUrl[this.finalUrl.length - 1] !== '?') {
      this.finalUrl += '?';
    }

    for (const [key, value] of Object.entries(params)) {
      if (this.finalUrl[this.finalUrl.length - 1] === '?') {
        this.finalUrl += `${key}=${value}`;
      } else {
        this.finalUrl += `&${key}=${value}`;
      }
    }

    return this;
  }

  build() {
    return this.finalUrl;
  }
}
