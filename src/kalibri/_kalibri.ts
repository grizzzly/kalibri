import {
  IAfterInterceptor,
  IBeforeInterceptor,
  IDeleteRequestConfig,
  IFinalClientConfig,
  IGetRequestConfig,
  IPostRequestConfig,
  IRequestConfig,
  ISuccessResponse,
  REQUEST_METHODS,
} from './_types';

import { isServer } from './_helpers';
import { URLBuilder } from './_url.builder';
import ClientAdapter from './adapters/_client.adapter';
import ServerAdapter from './adapters/_server.adapter';
import kalibri from '.';

export interface IKalibriConfig {
  baseUrl: string;
  timeout: number;
  headers: { [n: string]: string };
  ignoredHeaders: { key: string; times: number }[];
}

export const initConfig: IKalibriConfig = {
  baseUrl: '/',
  timeout: 10000,
  headers: {},
  ignoredHeaders: [],
};

export class Kalibri {
  private config: IKalibriConfig = initConfig;
  protected beforeRequestInterceptor: IBeforeInterceptor = (
    config: IFinalClientConfig,
  ): void => {};
  protected afterRequestInterceptor: IAfterInterceptor = (
    requestResult: ISuccessResponse<any>,
  ) => {};

  constructor({
    baseUrl = '/',
    timeout = 10000,
    headers = {},
  }: Partial<Omit<IKalibriConfig, 'ignoredHeaders'>> = initConfig) {
    if (typeof baseUrl !== 'string') {
      throw new Error(`Expected baseUrl as string but got ${typeof baseUrl}`);
    }

    if (
      headers === null ||
      typeof headers !== 'object' ||
      Array.isArray(headers)
    ) {
      throw new Error(`Expected headers as array but got ${typeof headers}`);
    }

    if (typeof timeout !== 'number') {
      throw new Error(`Expected timeout as number but got ${typeof timeout}`);
    }

    this.config = {
      baseUrl: baseUrl || initConfig.baseUrl,
      timeout: timeout || initConfig.timeout,
      headers: headers || initConfig.headers,
      ignoredHeaders: [],
    };
  }

  private get headers(): {} {
    const $headers = { ...this.config.headers };
    const $newIgnoredHeaders = [];

    for (const ignoredHeader of this.config.ignoredHeaders) {
      const { key, times } = ignoredHeader;

      if (key in $headers) {
        delete $headers[key];
      }

      ignoredHeader.times = times - 1;

      if (ignoredHeader.times > 0) {
        $newIgnoredHeaders.push(ignoredHeader);
      }
    }

    this.config.ignoredHeaders = $newIgnoredHeaders;

    return $headers;
  }

  createInstance(
    options?: Partial<Omit<IKalibriConfig, 'ignoredHeaders'>>,
  ): Kalibri {
    return new Kalibri(options);
  }

  getConfig(): IKalibriConfig {
    return JSON.parse(JSON.stringify(this.config));
  }

  // set kalibri request url
  setBaseUrl(url: string): void {
    if (url[url.length - 1] !== '/') {
      url += '/';
    }

    this.config.baseUrl = url;
  }

  addHeaders(headers: { [n: string]: string }) {
    if (headers === null || typeof headers !== 'object') {
      throw new Error(`Exprcted headers as object, but got: ${headers}`);
    }

    this.config.headers = {
      ...this.config.headers,
      ...headers,
    };
  }

  removeHeaders(headers: string[]): { [K in typeof headers[number]]: string };
  removeHeaders(headers: string): string;
  removeHeaders(headers: any): any {
    const removeHeader = (key: string): string => {
      const headerText = this.config.headers[key];
      delete this.config.headers[key];
      return headerText;
    };

    if (typeof headers === 'string') {
      return removeHeader(headers);
    }

    if (Array.isArray(headers)) {
      const res: { [K in typeof headers[number]]: string } = {};

      headers.forEach((key) => {
        const headerText = removeHeader(key);
        res[key] = headerText;
      });

      return res;
    }

    throw new Error('');
  }

  setAfterRequestInterceptor(afterRequestInterceptor: IAfterInterceptor) {
    this.afterRequestInterceptor = afterRequestInterceptor;
  }

  setBeforeRequestInterceptor(beforeRequestInterceptor: IBeforeInterceptor) {
    this.beforeRequestInterceptor = beforeRequestInterceptor;
  }

  ignoreHeaders(ignoredHeaders: { key: string; times: number }[]) {
    this.config.ignoredHeaders = [
      ...this.config.ignoredHeaders,
      ...ignoredHeaders,
    ];
  }

  stopIgonringHeaders(keys: string | string[]) {
    if (typeof keys === 'string') {
      this.removeIgnoredHeader(keys);
    }

    if (Array.isArray(keys)) {
      keys.forEach((key) => {
        this.removeIgnoredHeader(key);
      });
    }
  }

  private removeIgnoredHeader(key: string) {
    this.config.ignoredHeaders = this.config.ignoredHeaders.filter(
      (ignoredHeader) => ignoredHeader.key !== key,
    );
  }

  request<T>(
    requestConfig: Partial<IRequestConfig>,
  ): Promise<ISuccessResponse<T>> {
    const $requestConfig: IRequestConfig = {
      method: requestConfig.method || REQUEST_METHODS.GET,
      url: new URLBuilder(this.config.baseUrl)
        .path(requestConfig.url || '')
        .query(requestConfig.query || {})
        .build(),
      headers: { ...this.headers, ...requestConfig.headers },
      query: requestConfig.query || {},
      timeout: requestConfig.timeout || this.config.timeout,
      body: requestConfig.body,
      responseType: requestConfig.responseType || '',
    };

    const adapter = isServer() ? new ServerAdapter() : new ClientAdapter();

    return adapter.request({
      requestConfig: $requestConfig,
      interceptors: {
        beforeRequestInterceptor: this.beforeRequestInterceptor,
        afterRequestInterceptor: this.afterRequestInterceptor,
      },
    });
  }

  get<T>(url: string, requestConfig: IGetRequestConfig = {}) {
    return this.request<T>({
      ...requestConfig,
      url,
      method: REQUEST_METHODS.GET,
    });
  }

  post<T>(url: string, requestConfig: IPostRequestConfig = {}) {
    return this.request<T>({
      ...requestConfig,
      url,
      method: REQUEST_METHODS.POST,
    });
  }

  patch<T>(url: string, requestConfig: IPostRequestConfig = {}) {
    return this.request<T>({
      ...requestConfig,
      url,
      method: REQUEST_METHODS.PATCH,
    });
  }

  put<T>(url: string, requestConfig: IPostRequestConfig = {}) {
    return this.request<T>({
      ...requestConfig,
      url,
      method: REQUEST_METHODS.PUT,
    });
  }

  delete<T>(url: string, requestConfig: IDeleteRequestConfig = {}) {
    return this.request<T>({
      ...requestConfig,
      url,
      method: REQUEST_METHODS.DELETE,
    });
  }
}
