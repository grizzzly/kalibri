export enum REQUEST_METHODS {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
  PUT = 'PUT',
}

export interface AnyObject {
  [name: string]: any;
}

export interface InitialConfig {
  baseUrl: string;
  timeout: number;
  lsHeadersKeys: string[];
}

export interface IRequestConfig {
  method: REQUEST_METHODS;
  url: string;
  headers: AnyObject;
  query: AnyObject;
  timeout: number;
  body: any;
  responseType: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | '';
}

export type IGetRequestConfig = Partial<
  Pick<IRequestConfig, 'headers' | 'query' | 'timeout' | 'responseType'>
>;

export type IPostRequestConfig = Partial<
  Pick<
    IRequestConfig,
    'headers' | 'query' | 'timeout' | 'body' | 'responseType' | 'responseType'
  >
>;

export type IPatchRequestConfig = Partial<
  Pick<
    IRequestConfig,
    'headers' | 'query' | 'timeout' | 'body' | 'responseType'
  >
>;

export type IDeleteRequestConfig = Partial<
  Pick<IRequestConfig, 'headers' | 'query' | 'timeout' | 'responseType'>
>;

export type IFinalClientConfig = Omit<IRequestConfig, 'query'>;

export type IAdapterRequestConfig = Omit<IRequestConfig, 'query'>;

export interface IFinalServerConfig {
  origin: string;
  hostname: string;
  port?: number;
  path?: string;
  headers?: AnyObject;
  timeout: number;
  method: REQUEST_METHODS;
  data: Uint8Array | null;
  responseType: IRequestConfig['responseType'];
}
export type GetRequest = (url: string, config?: IGetRequestConfig) => void;

export type PostRequest = (url: string, config?: IPostRequestConfig) => void;

export type PatchRequest = (url: string, config?: IPatchRequestConfig) => void;

export type DeleteRequest = (
  url: string,
  config?: IDeleteRequestConfig,
) => void;

export interface ISuccessResponse<T> {
  ok: boolean;
  status: number;
  statusText: string;
  headers: object | string;
  data: T;
}

export interface IErrorResponse {
  ok: boolean;
  status: number;
  statusText: string;
  headers: object | string;
  message: string;
  data: string;
}

export type IBeforeInterceptor = (config: IFinalClientConfig) => void;

export type IAfterInterceptor = (requestResult: ISuccessResponse<any>) => void;

export interface IKalibri {
  setBeforeRequestInterceptor: (interceptor: IBeforeInterceptor) => void;
  setAfterRequestInterceptor: (interceptor: IAfterInterceptor) => void;
  get: GetRequest;
  post: PostRequest;
  patch: PatchRequest;
  delete: DeleteRequest;
}

export interface ILsHeader {
  [title: string]: string;
}

export interface ILsKalibri {
  constantHeaders: ILsHeader;
  sessionHeaders: ILsHeader;
}
