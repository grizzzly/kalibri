import * as http from 'http';
import * as https from 'https';
import { IAdapterRequestConfig } from '..';
import {
  IErrorResponse,
  IFinalServerConfig,
  ISuccessResponse,
} from '../_types';
import { AAdapter, IAdapterRequestOptions } from './_base.adapter';

const getHttp = (url: string) => {
  return url.includes('https://') ? https : http;
};

export default class ServerAdapter extends AAdapter {
  private parseResponseText(data: string, headers: http.IncomingHttpHeaders) {
    try {
      if (
        headers['content-type'] === 'application/json' ||
        data[0] === '{' ||
        data[0] === '['
      ) {
        return JSON.parse(data);
      } else {
        return data;
      }
    } catch (err) {
      return data;
    }
  }

  private createSuccessResponse<T>(
    data: Buffer,
    res: http.IncomingMessage,
  ): ISuccessResponse<T> {
    const { statusCode = 0, headers, statusMessage: statusText = '' } = res;

    return {
      headers,
      statusText,
      ok: true,
      status: statusCode,
      data: this.parseResponseText(data.toString(), res.headers),
    };
  }

  private createErrorResponse(
    res: http.IncomingMessage,
    data: Buffer,
    message: string = '',
  ): IErrorResponse {
    const {
      statusCode: status = 0,
      statusMessage: statusText = '',
      headers,
    } = res;
    return {
      ok: false,
      status,
      statusText,
      headers,
      message,
      data: this.parseResponseText(data.toString(), res.headers),
    };
  }

  private createFinalConfig(
    requestConfig: IAdapterRequestConfig,
  ): IFinalServerConfig {
    const {
      url,
      method,
      headers,
      timeout,
      body = {},
      responseType,
    } = requestConfig;

    const finalUrl = new URL(url);

    const data =
      typeof body === 'object' && Object.entries(body).length
        ? new TextEncoder().encode(
            JSON.stringify({
              ...body,
            }),
          )
        : null;

    return {
      data,
      method,
      timeout,
      headers,
      responseType,
      origin: finalUrl.origin,
      hostname: finalUrl.hostname,
      path: finalUrl.pathname + finalUrl.search,
      port: finalUrl.port ? parseInt(finalUrl.port, 10) : undefined,
    };
  }

  request<T>({
    requestConfig,
    interceptors: { beforeRequestInterceptor, afterRequestInterceptor },
  }: IAdapterRequestOptions): Promise<ISuccessResponse<T>> {
    beforeRequestInterceptor(requestConfig);

    const options = this.createFinalConfig(requestConfig);

    if (options.data !== null) {
      Object.assign(options, {
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': options.data.length,
        },
      });
    }

    return new Promise((resolve, reject) => {
      const req = getHttp(options.origin).request(options, (res) => {
        res.on('data', (data) => {
          const { statusCode = 0 } = res;
          if (statusCode >= 200 && statusCode < 400) {
            return resolve(this.createSuccessResponse<T>(data, res));
          }

          reject(this.createErrorResponse(res, data));
        });
      });

      req.on('error', (error) => {
        reject(error);
      });

      if (options.data) {
        req.write(options.data);
      }

      req.end();
    });
  }
}
