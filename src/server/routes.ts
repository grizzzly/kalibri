import { Router } from 'express';
import * as multer from 'multer';

const router = Router();
const upload = multer();

router.all('/test', upload.single('image'), async (req, res) => {
  const resBody = {
    headers: req.headers,
    status: 'ok',
  };
  if (typeof req.body === 'object') Object.assign(resBody, req.body);

  if (req.file)
    Object.assign(resBody, {
      fileName: req.file.originalname,
      file: req.file.buffer.toString('base64'),
    });

  res.json(resBody);
});

router.post('/error', (req, res) => {
  res.status(404);
  res.json({ status: 'error' });
});

export default router;
