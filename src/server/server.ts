import * as express from 'express';
import * as cors from 'cors';
import router from './routes';
import { Server } from 'http';
import kalibri from '..';

const options: cors.CorsOptions = {
  allowedHeaders: [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'X-Access-Token',
    'jwt',
  ],
  credentials: true,
  methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
  origin: ['http://localhost:3000', 'http://localhost', '*'], // 'http://localhost' for test
  preflightContinue: false,
};

export const createServer = (PORT: number) => {
  let serverInstance: Server;
  const app = express();

  app.use(cors());
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  app.use(router);

  async function start() {
    serverInstance = await app.listen(PORT, () => {
      console.log(`App is running on http://localhost:${PORT}`);
      kalibri.get('http://localhost:3001/test').then((res) => {
        console.log(res);
      });
    });
  }

  async function close() {
    await serverInstance.close();
  }

  return {
    app,
    start,
    close,
  };
};
