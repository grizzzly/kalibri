import { createServer } from './server';

const server = createServer(3001);

server.start();
