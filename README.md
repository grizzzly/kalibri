# Kalibri

[![npm version](https://badgen.net/npm/v/kalibri)](https://www.npmjs.com/package/kalibri)
[![install size](https://badgen.net/packagephobia/install/kalibri)](https://packagephobia.com/result?p=kalibri)
[![Gzip size](https://badgen.net/bundlephobia/minzip/kalibri)](https://bundlephobia.com/result?p=kalibri)

Kalibri - легковесная библиотека для выполнения запросов как с клиента, так и с сервера

## Navigation

- [Добавление в проект](#добавление-в-проект)
- [Использование](#использование)
- [Примеры](#примеры)

## Добавление в проект

npm:

```bash
npm install kalibri
```

yarn:

```bash
yarn add kalibri
```

## Использование

### Начало работы

Первым делом необходимо импортировать kalibri:

```js
import kalibri from 'kalibri';
```

Можно создать новый объект:

```js
import kalibri from 'kalibri';

const customHttp = kalibri.createInstance({
  baseUrl: 'https://jsonplaceholder.typicode.com', // default '/'
  headers: {}, // default {}
  timeout: 20000, // default 10000
});

kalibri.getConfig(); // возвращает все настройки
// {
//   baseUrl: 'https://jsonplaceholder.typicode.com',
//   timeout: 20000,
//   headers: {},
//   ignoredHeaders: []
//   }
```

или использовать импортированный.

Задаем осоновной адрес:

```js
import kalibri from 'kalibri';

kalibri.setBaseUrl('https://example.com');
```

### Заголовки

Для того, чтобы добавлять и убирать заголовки, следует использовать следующие методы:

```js
kalibri.addHeaders({
  'Header title': 'Header body',
});

kalibri.getConfig().headers; // вернет { 'Header title': 'Header body' }

kalibri.removeHeaders('Header title'); // также можно передать массив

kalibri.getConfig().headers; // вернет {}
```

Также заголовки можно добавлять в исключения на определенное количество запросов:

```js
kalibri.ignoreHeaders([
  {
    key: 'Header title', // название заголовка
    times: 2, // количество запросов, в которых заголовок не будет указан
  },
]);
kalibri.getConfig().ignoredHeaders; // вернет [{key: 'Header title', times: 2}]
```

## Примеры

Этот раздел еще не готов...