import 'regenerator-runtime';

import kalibri, { Kalibri } from '../../../src/kalibri';

declare global {
  interface Window {
    kalibri: Kalibri;
  }
}

(async function () {
  window.kalibri = kalibri;

  kalibri.ignoreHeaders([{ key: 'Cont', times: 1 }]);

  kalibri.addHeaders({
    'Header title': 'Header body',
  });
  console.log(kalibri.getConfig().headers);

  kalibri.removeHeaders('Header title');
  console.log(kalibri.getConfig().headers);

  kalibri.ignoreHeaders([
    {
      key: 'Header title',
      times: 2,
    },
  ]);
  console.log(kalibri.getConfig().ignoredHeaders);

  const customHttp = kalibri.createInstance({
    baseUrl: 'https://jsonplaceholder.typicode.com', // default '/'
    headers: {}, // default {}
    timeout: 10000, // default
  });

  try {
    const { ok, data, status, statusText } = await customHttp.get('/posts');
    console.log(ok, status, statusText);
    console.log(data);
  } catch (err) {
    console.log(err);
  }

  try {
    const { ok, data, status, statusText } = await customHttp.post('/posts', {
      body: { id: 1, title: 'Some title', description: 'Some description' },
    });
    console.log(ok, status, statusText);
    console.log(data);
  } catch (err) {
    console.log(err);
  }

  try {
    const { ok, data, status, statusText } = await customHttp.delete(
      '/posts/1',
    );
    console.log(ok, status, statusText);
    console.log(data);
  } catch (err) {
    console.log(err);
  }

  customHttp.setAfterRequestInterceptor(() => {
    console.warn('Err');
  });

  try {
    await customHttp.post('/error');
  } catch (err) {
    console.log(err);
  }
})();
